FROM centos:7

LABEL maintainer=Hlulani \
      name=389-ds \
      twitter=@mhlulani \
      build-date=20180621

ENV container docker

RUN  yum -y install 389-ds-base && \ 
      rm -fr /usr/lib/systemd/system && \
      sed -i 's/updateSelinuxPolicy($inf);//g' /usr/lib64/dirsrv/perl/*

EXPOSE 389 9830

COPY scripts/entrypoint.sh /bin/entrypoint.sh

RUN chown dirsrv:dirsrv /bin/entrypoint.sh && \
      chmod +x /bin/entrypoint.sh && \
      touch /usr/share/dirsrv/restore.ldif

VOLUME ["/etc/dirsrv","/var/lib/dirsrv","/var/log/dirsrv"]

CMD [ "/bin/entrypoint.sh" ]
