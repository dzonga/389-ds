#!/bin/bash
set -e

DIRSRV_FQDN=${DIRSRV_FQDN:-example.org}
DIRSRV_SUFFIX=${DIRSRV_SUFFIX:-dc=example,dc=org}
DIRSRV_PORT=${DIRSRV_PORT:-389}
DIRSRV_ROOT_DN=${DIRSRV_ROOT_DN:-cn=Directory Manager}
DIRSRV_ROOT_DN_PASSWORD=${DIRSRV_ROOT_DN_PASSWORD:-topsecret}
DIRSRV_ADMIN_PORT=${DIRSRV_ADMIN_PORT:-9830}
DIRSRV_ID=${DIRSRV_ID:-dir}
DIRSRV_ADMIN_USERNAME=${DIRSRV_ADMIN_USERNAME:-admin}
DIRSRV_ADMIN_PASSWORD=${DIRSRV_ADMIN_PASSWORD:-adminpassword}
DIRSRV_BASE=/etc/dirsrv/slapd-$DIRSRV_ID
DIRSRV_ORG_ENTRIES=${DIRSRV_ORG_ENTRIES:-no}
DIRSRV_SAMPLE_ENTRIES=${DIRSRV_SAMPLE_ENTRIES:-no}

# slapd.InstallLdifFile=/etc/dirsrv/restore.ldif and slapd.AddOrgEntries=yes are mutually exclusive
if [[ "$DIRSRV_ORG_ENTRIES" == "yes" || "$DIRSRV_ORG_ENTRIES" == "Yes" ]]; then 
DIRSRV_INSTALL_LDIF=suggest; 
else 
  DIRSRV_INSTALL_LDIF=/usr/share/dirsrv/restore.ldif
  if [ ! -s "$DIRSRV_INSTALL_LDIF" ]; then
    echo "'$DIRSRV_INSTALL_LDIF' does not exist, or is empty"
    exit 1
  fi
fi

# TODO: there is still a couple of tests I need to do here
if [ ! -d "$DIRSRV_BASE" ]; then
echo "Configuring ${DIRSRV_ID}..."
# This always fails because it's trying to use Systemd which is disabled by default
# So I'll run anyway just to create the relevant configs/paths etc. 
# (this should not take more that 4 seconds)
timeout 10 /usr/sbin/setup-ds.pl --silent --debug \
      "General.FullMachineName=$DIRSRV_FQDN" \
      "General.StrictHostCheck=false" \
      "General.SuiteSpotUserID=dirsrv" \
      "General.SuiteSpotGroup=dirsrv" \
      "General.ConfigDirectoryAdminID=$DIRSRV_ADMIN_USERNAME" \
      "General.ConfigDirectoryAdminPwd=$DIRSRV_ADMIN_PASSWORD" \
      "slapd.ServerPort=$DIRSRV_PORT" \
      "slapd.ServerIdentifier=$DIRSRV_ID" \
      "slapd.Suffix=$DIRSRV_SUFFIX" \
      "slapd.RootDN=$DIRSRV_ROOT_DN" \
      "slapd.RootDNPwd=$DIRSRV_ROOT_DN_PASSWORD" \
      "slapd.AddSampleEntries=$DIRSRV_SAMPLE_ENTRIES" \
      "slapd.AddOrgEntries=$DIRSRV_ORG_ENTRIES" \
      "slapd.InstallLdifFile=$DIRSRV_INSTALL_LDIF" \
      "admin.Port=$DIRSRV_ADMIN_PORT" \
      "admin.ServerAdminID=$DIRSRV_ADMIN_USERNAME" \
      "admin.ServerAdminPwd=$DIRSRV_ADMIN_PASSWORD" || ERROR_CODE=$? # Ignoring errors, I'm expecting errors here

  # if it's a timeout code then it's more likely a known issue as described above
  # if [[ ! "$ERROR_CODE" -eq 124 ]]; then
  #   echo "There was a problem while configuring $DIRSRV_ID"
  #   exit 1
  # fi
echo "...(code $ERROR_CODE) configured ${DIRSRV_ID}, if there are any issues please have a look at the logs above"
fi
# Run the DIR Server
exec /usr/sbin/ns-slapd -D ${DIRSRV_BASE} -d 0 && tail -F /var/log/dirsrv/slapd-${DIRSRV_ID}/errors
