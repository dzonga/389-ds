# 389 Directory Server

The docker image for installing and running [389 DS](http://directory.fedoraproject.org/), inspired by project https://github.com/minkwe/389ds.
This is an attempt for a more flexible docker image that uses no additional utilities to get the job done.
Docker containers can be ran in one of two ways:

- General Usage
  - This requires `DIRSRV_ORG_ENTRIES=yes` to be set, this uses the internal data samples to prepopulate your LDAP database, with sample data
- Using Custom LDIF Install File
  - This does not make any assumptions on what kind of tree structure (DIT) your database will have, it gives you the freedom to define exactly what you will need. This requires that you use the *Bind Mount* `-v ~/foo/bar/restore.ldif:/usr/share/dirsrv/restore.ldif` when running the `Docker` container, please see instrution below

**NOTE: `-e DIRSRV_ORG_ENTRIES=yes` and `-v ~/foo/bar/restore.ldif:/usr/share/dirsrv/restore.ldif` are mutually exclusive, meaning you can have one or the other NOT both**

# Source Code
https://github.com/novalinc/389-ds

# Environment Variables
- `DIRSRV_FQDN`: the hostname to be used with Directory Server (default = example.org)
- `DIRSRV_SUFFIX`: the directory suffix (default = dc=example,dc=org}
- `DIRSRV_PORT`: LDAP Port (Default= 389)
- `DIRSRV_ROOT_DN`: Root distinguished name (DN) (default = 'cn=Directory Manager')
- `DIRSRV_ROOT_DN_PASSWORD`: Root DN password (default = topsecret)
- `DIRSRV_ADMIN_PORT`: this is for the console (default = 9830)
- `DIRSRV_ID`: instance name (default=dir)
- `DIRSRV_ADMIN_USERNAME`: admin username (default: admin)
- `DIRSRV_ADMIN_PASSWORD`: admin password (default: adminpassword)
- `DIRSRV_ORG_ENTRIES`: If *yes*, this directive creates the new Directory Server instance with a suggested directory structure and access control. If this directive is used and *Bind Mount* `/usr/share/dirsrv/restore.ldif` (internally uses `InstallLdifFile`) is also used, then this directive has no effect (default = no)
- `DIRSRV_SAMPLE_ENTRIES`: Sets whether to load an LDIF file with entries for the user directory during configuration (default = no).

# Volume Mounts
The following are the directories that are exposed and available to be mounted for persistence, by default no persistence is guranteed

- `/etc/dirsrv` - the location of instance and configuration data
- `/var/lib/dirsrv` - the location of directory server DB
- `/var/log/dirsrv` -  the logs of the directory server
- `/usr/share/dirsrv/restore.ldif` - location of restore file LDIF, use bind mount to link to this file from your host

# General Usage
To run the application using a locally installed docker

```bash
$ docker run --name 389ds -p 389:389 \
-e DIRSRV_ORG_ENTRIES=yes \
-e DIRSRV_FQDN=example.org \
-e DIRSRV_SUFFIX=dc=example,dc=org \
novalinc/389ds
```

# Using Custom LDIF Install File
```bash
    # Here you can choose the location you want
$ mkdir -p ~/foo/bar

    # Create a new file, NOTE: the name of the file is important and MUST be 'restore.ldif'
$ vi ~/foo/bar/restore.ldif
```
Then copy and paste the following into your `restore.ldif` file, remembering that all references of `dc=example,dc=org` in this file **MUST** match what is defined by the `$DIRSRV_SUFFIX` environmental variable

```ldif
dn: dc=example,dc=org
objectClass: dcObject
objectClass: organization
description: This is my kickass organization 
o: Example Inc.

dn: ou=person,dc=example,dc=org
objectclass: organizationalUnit
ou: person

dn: uid=azania,ou=person,dc=example,dc=org
objectclass: inetOrgPerson
uid: azania
cn: Azania
sn: Themba
mail: azania@yahoo.com
postalCode: 88441
description: United we shall stand
userPassword: {SHA}EiAf5eICiDvUX8l+hzZuoFGD4OQ=

dn: uid=hlamalani,ou=person,dc=example,dc=org
objectclass: inetOrgPerson
uid: hlamalani
cn: Hlamalani
sn: Higgins
mail: hlamalani@webmail.com
postalCode: 88441
description: Live and strive for freedom
userPassword: {SHA}EiAf5eICiDvUX8l+hzZuoFGD4OQ=

dn: ou=role,dc=example,dc=org
objectclass: organizationalUnit
ou: role

dn: cn=master,ou=role,dc=example,dc=org
objectclass: groupOfNames
cn: master
description: Master is the boss
member: uid=hlulani,ou=person,dc=example,dc=org

dn: cn=slave,ou=role,dc=example,dc=org
objectclass: groupOfNames
cn: slave
description: Minion to the boss
member: uid=hlamalani,ou=person,dc=example,dc=org
```
Note that passwords should be hashed using the following one of the commands (see [here](https://access.redhat.com/documentation/en-us/red_hat_directory_server/10/html/configuration_command_and_file_reference/plug_in_implemented_server_functionality_reference#password-storage-schemes) for more storage schemes):

```bash
# NOTE: you need to have the openldap-clients install to run this command
$ pwdhash -s SHA topsecret
# OUTPUT: {SHA}EiAf5eICiDvUX8l+hzZuoFGD4OQ=

$ pwdhash topsecret
# OUTPUT: {SSHA512}RKfO7CkD/xO0LOssWFdcRBTmMyD7t0XYh6fYfB2dPzZI+7BxoAZOYONOQDh0IcRlA8w20Qz2N4naXSzToJcj5b1d9OUXWxFX
```

Run the following command
```bash
$ docker run -p 389:389 --rm --name ldap \
-e DIRSRV_FQDN=example.org \
-e DIRSRV_SUFFIX=dc=example,dc=org \
-v ~/foo/bar/restore.ldif:/usr/share/dirsrv/restore.ldif \
novalinc/389ds
```
Verify by looking for the log that says:
```
+[24/Jun/2018:08:04:03.958823374 +0000] - INFO - import_producer - import userRoot: Processing file "/usr/share/dirsrv/restore.ldif"
+[24/Jun/2018:08:04:03.962404179 +0000] - INFO - import_producer - import userRoot: Finished scanning file "/usr/share/dirsrv/restore.ldif" (7 entries)
...
+[24/Jun/2018:08:04:04.608737744 +0000] - INFO - import_main_offline - import userRoot: Import complete.  Processed 7 entries in 1 seconds. (7.00 entries/sec)
```
you can also use `docker container logs <containername>` this is useful when you are running with `--detach`